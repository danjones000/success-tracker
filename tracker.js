'use strict';

const path = require('path');
const fs = require('fs');
var Promise = require('bluebird');

const readFile = Promise.promisify(fs.readFile);
const writeFile = Promise.promisify(fs.writeFile);

module.exports = function tracker(configFile) {
  let config = path.resolve(configFile);

  let read = function () {
    return readFile(config, {encoding: 'utf-8'}).catch(function (err) {
      if (err.code === 'ENOENT') return Promise.resolve(JSON.stringify({}));

      return Promise.reject(err);
    }).then(function (data) {
      let obj;

      try { obj = JSON.parse(data); }
      catch (e) { obj = {}; }
      if (typeof obj !== 'object') obj = {};

      obj.successes = obj.successes || 0;
      obj.lastReset = obj.lastReset || Date.now();

      return Promise.resolve(obj);
    });
  };

  let write = function (modifier) {
    return read().then(
      (data => Promise.resolve(modifier(data)))
    ).then(
      (data => {writeFile(config, JSON.stringify(data), 'utf-8'); return Promise.resolve(data);})
    );
  };

  let addSuccess = function () {
    return write(data => {++data.successes; return data;});
  };

  let successes = function () {
    return read().then(data => Promise.resolve(data.successes));
  };

  let lastReset = function () {
    return read().then(
      data => Promise.resolve(new Date(data.lastReset))
    );
  };

  let reset = function () {
    return write(() => ({successes: 0, lastReset: Date.now()}));
  };

  return {
    read: function () {
      return read().then(function (data) {
        data.lastReset = new Date(data.lastReset);

        return Promise.resolve(data);
      });
    },
    successes,
    lastReset,
    addSuccess,
    reset
  };
};
