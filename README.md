# Success Tracker

Success Tracker can be used to track your success in overcoming bad habits.

Instead of simply tracking the number of days since you last gave in to the habit, you track the number of times you
didn't give in to your habit.

Let's say you're trying to stop biting your fingernails. You start out by recording the last time you did bite your
fingernails. Then, after that, every time you want to bite your fingernails, but you don't, you add one to your
tracker. Anytime you want, you can check your tracker and see the number of times you've beaten your habit. Seeing this
number can reinforce your resolve. Instead of saying, "Oh, it's only been one day", you look at your number and say to
yourself, "I've already beaten this habit ten times, I don't want to give in now!"

If you do give in, and bite your fingernails, you reset your tracker and try again.

## Current status

This is still very much a work in progress in the very early stages. This is intended to be a web app, but currently, it
works as a command-line program. There's not a fancy way to install or use it. The easiest way currently is the
following:

```shell
npm install -g git+https://gitlab.com/danjones000/success-tracker.git
success-tracker info
success-tracker add # Adds a success
success-tracker -t /path/to/tracker_file.json info
```

`success-tracker --help` will give the command-line options.

A better installer will be developed.
