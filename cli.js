#!/usr/bin/env node

const print = console.log;

const successes = function (argv) {
  argv.tracker.successes().then(s => print(s));
};

const add = function (argv) {
  argv.tracker.addSuccess().then(data => print(data.successes));
};

const info = function (argv) {
  argv.tracker.read().then(function (data) {
	print(`${data.successes} successes since ${data.lastReset.toLocaleString()}`);
  });
};

const reset = function (argv) {
  argv.tracker.reset().then(() => print(`Reset successes now at ${(new Date()).toLocaleString()}`));
};

const argv = require('yargs')
  .option('t', {
	alias: 'tracker',
	default: 'settings.json',
	describe: 'Path to success tracker file',
	coerce: function(file) {
	  return require('./tracker')(file);
	}
  })
  .command('*', 'Print current status of successes', () => {}, info)
  .command('add', 'Add another success', () => {}, add)
  .command('successes', 'Print current number of successes', () => {}, successes)
  .command('reset', 'Reset your successes to now', () => {}, reset)
  .help()
  .argv;
