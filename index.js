'use strict';

const express = require('express');
const app = express();
const port = process.env.PORT || 4444;

app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.listen(port, () => console.log(`Example app listening on port ${port}!`));
